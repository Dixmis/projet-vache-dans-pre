/** 
 * Calcul l'aire du pré
 * @param {boolean} unsigned - Si on souhaite la valeur signée ou non de l'aire du pré
 * @return {number} Aire du pré
 */
function calculateAire( unsigned = true ) {
    coords = getValues( ); // On récupère les coordonnées des piquets
    aire = 0;
    for ( i = 0; i < coords.length; i++ ) {
        ip1 = ( i + 1 ) % coords.length;
        aire += ( coords[ i ].x * coords[ ip1 ].y - coords[ i ].y * coords[ ip1 ].x ); // On applique la formule
    }
    if ( unsigned )
        return round( Math.abs( aire / 2 ), 3 ); // Les nombres à virgule sont très bizarres en Javascript donc on arrondi tout à 10^-3
    else
        return round( ( aire / 2 ), 3 );
}

/** 
 * Calcul du centre de gravité du pré (position de la vache)
 * @return {{x:number, y:number}} Coordonnées du centre du gravité
 */
function calculateGravite( ) {
    coords = getValues( );
    aire = calculateAire( false ); // On récupère l'aire signée
    Gx = 0;
    Gy = 0;
    for ( i = 0; i < coords.length; i++ ) {
        ip1 = ( i + 1 ) % coords.length;
        Gx += ( coords[ i ].x + coords[ ip1 ].x ) * ( coords[ i ].x * coords[ ip1 ].y - coords[ i ].y * coords[ ip1 ].x ); // On applique la formule
        Gy += ( coords[ i ].y + coords[ ip1 ].y ) * ( coords[ i ].x * coords[ ip1 ].y - coords[ i ].y * coords[ ip1 ].x );
    }
    return { x: round( ( 1 / ( 6 * aire ) ) * Gx, 3 ), y: round( ( 1 / ( 6 * aire ) ) * Gy, 3 ) }; // On retourne les valeurs dans un dictionnaire
}

/** 
 * Calcul si la vache est dans le pré ou non
 * @return {boolean} True si la vache est dans le pré, false sinon.
 */
function calculateInsidePolygon( ) {
    gravit = calculateGravite( );
    somme = 0;
    for ( i = 0; i < getNbPiquets( ); i++ ) {
        somme += calculateAngle( i, gravit ); // On applique la formule
    }
    return ( round( somme, 3 ) != 0 ); // On teste si la somme est différente de 0 ou pas et on renvoi le résultat du test
}

/** 
 * Calcul un angle entre 2 piquets du pré par rapport au centre de gravité
 * @param {number} index - Index du premier piquet
 * @param {{x:number,y:number}} gravit - Centre de gravité
 * @return {number} Angle entre les deux piquet par rapport au centre de gravité.
 */
function calculateAngle( index, gravit ) {
    pointi = getPoint( index ); // On récupère les coordonnées des points
    pointip1 = getPoint( index + 1 );
    vectorGSi = vector( gravit, pointi ); // On créer les vecteurs
    vectorGsip1 = vector( gravit, pointip1 );
    return Math.acos( // Calcul du arccosinus
            vectProd( vectorGSi, vectorGsip1 ) / // Application de la formule
            (
                vectNorm( vectorGSi ) *
                vectNorm( vectorGsip1 )
            )
        ) *
        ( vectDet( vectorGSi, vectorGsip1 ) > 0 ? 1 : -1 ); // Si le déterminant est positif alors *1 mais s'il est négatif alors *-1 pour utiliser uniquement le signe du déterminant
}

/** 
 * Créer un vecteur entre 2 points
 * @param {{x:number,y:number}} point1 - Point d'origine du vecteur.
 * @param {{x:number,y:number}} point2 - Point de destination du vecteur.
 * @return {{x:number,y:number}} Vecteur
 */
function vector( point1, point2 ) {
    xcoord = point2.x - point1.x;
    ycoord = point2.y - point1.y;
    return { x: xcoord, y: ycoord }; // Création du vecteur en application de la formule
}

/** 
 * Calcul le produit vectoriel entre 2 vecteurs
 * @param {{x:number,y:number}} vector1 - Vecteur 1.
 * @param {{x:number,y:number}} vector2 - Vecteur 2.
 * @return {number} Produit vectoriel.
 */
function vectProd( vector1, vector2 ) {
    return ( vector1.x * vector2.x + vector1.y * vector2.y ); // Application de la formule
}

/** 
 * Calcul la normale d'un vecteur
 * @param {{x:number,y:number}} vector - Vecteur
 * @return {number} Normale du vecteur
 */
function vectNorm( vector ) {
    return Math.sqrt( Math.pow( vector.x, 2 ) + Math.pow( vector.y, 2 ) ); // Application de la formule
}

/** 
 * Calcul la normale d'un vecteur
 * @param {{x:number,y:number}} vector - Vecteur
 * @return {number} Normale du vecteur
 */
function vectDet( vector1, vector2 ) {
    return ( vector1.x * vector2.y - vector1.y * vector2.x ); // Application de la formule
}

/** 
 * Fonction de calcul de coordonnées d'un piquet dans le pré pour le replacer dans le canvas, le canvas ayant des coordonnées fixes.
 * Grâce à des calculs supplémentaires le ratio d'affichage est respecté ainsi que l'orientation des axes.
 * @param {{x:number,y:number}} origin - Coordonnées du point d'origine
 * @param {number} canvasWidth - Largeur en pixel du canvas ou coordonnée maximale en x si le canvas commence à 0
 * @param {number} canvasHeight - Hauteur en pixel du canvas ou coordonnée maximale en y si le canvas commence à 0
 * @param {number} padding - Marge intérieure qu'on souhaite ajouter pour éviter que les points aux extrémités se retrouvent à la limite
 * @return {{x:number,y:number}} Coordonnées du point dans le canvas
 */
function mapCoords( origin, canvasWidth, canvasHeight, padding ) {
    sourceBorneXMin = SearchXMin( ); // On récupère les extrémitées de chaque axe dans le pré
    sourceBorneYMin = SearchYMin( );
    sourceBorneXMax = SearchXMax( );
    sourceBorneYMax = SearchYMax( );
    originalWidth = sourceBorneXMax - sourceBorneXMin; // Calcul de la taille du rectangle dans lequel le pré est inscrit
    originalHeight = sourceBorneYMax - sourceBorneYMin;
    widthRatio = ( canvasWidth - 2 * padding ) / originalWidth; // On récupère le ratio canvas / pré
    heightRatio = ( canvasHeight - 2 * padding ) / originalHeight;
    borneXMin = padding;
    borneYMin = padding;
    if ( widthRatio > heightRatio ) { // Si la largeur est supérieure à la hauteur alors on la diminue pour éviter d'étirer le pré
        ratio = heightRatio;
        offset = ( ( canvasWidth - 2 * padding ) / 2 ) - ( originalWidth * ratio ) / 2; // On calcul de combien on doit décaler les points 
        borneXMin += offset; // On décale les points
    }
    if ( heightRatio > widthRatio ) { // Pareil mais pour la hauteur supérieure à la largeur
        ratio = widthRatio;
        offset = ( ( canvasHeight - 2 * padding ) / 2 ) - ( originalHeight * ratio ) / 2;
        borneYMin += offset;
    }
    newX = borneXMin + ratio * ( origin.x - sourceBorneXMin ); // On défini les nouvelles coordonnées grâces aux calculs précédent
    newY = borneYMin + ratio * ( origin.y - sourceBorneYMin );
    newY = canvasHeight - newY; // On retourne l'axe Y parce que le canvas commence en haut à gauche
    return { x: newX, y: newY }; // On renvoie les nouvelles coordonnées
}

/**
 * Calcul la coordonnée minimale pour l'axe y
 * @return {number} Coordonnée minimale pour l'axe y
 */
function SearchYMin( ) {
    coords = getAvailablesValues( );
    min = coords[ 0 ].y;
    for ( i = 1; i < coords.length; i++ ) {
        if ( min > coords[ i ].y ) min = coords[ i ].y;
    }
    return min;
}

/**
 * Calcul la coordonnée maximale pour l'axe y
 * @return {number} Coordonnée maximale pour l'axe y
 */
function SearchYMax( ) {
    coords = getAvailablesValues( );
    max = coords[ 0 ].y;
    for ( i = 1; i < coords.length; i++ ) {
        if ( max < coords[ i ].y ) max = coords[ i ].y;
    }
    return max;
}

/**
 * Calcul la coordonnée minimale pour l'axe x
 * @return {number} Coordonnée minimale pour l'axe x
 */
function SearchXMin( ) {
    coords = getAvailablesValues( );
    min = coords[ 0 ].x;
    for ( i = 1; i < coords.length; i++ ) {
        if ( min > coords[ i ].x ) min = coords[ i ].x;
    }
    return min;
}

/**
 * Calcul la coordonnée maximale pour l'axe x
 * @return {number} Coordonnée maximale pour l'axe x
 */
function SearchXMax( ) {
    coords = getAvailablesValues( );
    max = coords[ 0 ].x;
    for ( i = 1; i < coords.length; i++ ) {
        if ( max < coords[ i ].x ) max = coords[ i ].x;
    }
    return max;
}

/**
 * Arrondir un nombre à un certains nombre de décimales
 * @param {number} number - Nombre à arrondir
 * @param {number} decimals - Nombre de décimales
 * @return {number} Nombre arrondi
 */
function round( number, decimals ) {
    return Number( // Converti une chaine de caractère en nombre réel
        number.toFixed( decimals ) // écrit un nombre réel en chaine de caractère en fixant le nombre de décimales (2 devient 2.00 par exemple ou 3.1248 devient 3.12)
    );
}
