/**
 * Permet d'afficher le nombres de champs à afficher en fonction du nombre de piquet à placer
 */
function changeNbrPiquets( ) {
    for ( var i = 0, nombrePiquets = getNbPiquets( ) - 1; i < 50; i++ ) // Pour chaque champs disponibles
        if ( i > nombrePiquets ) { // Si le champs est supérieur au nombre de piquets choisi par l'utilisateur
            $( "#coords_" + i ).hide( ); // Alors on cache le champs en question
            $( "#coord_" + i + "_x" ).val( "" ); // On vide les champs x et y pour éviter que un mauvais code par la suite vienne prendre des valeurs erronées
            $( "#coord_" + i + "_y" ).val( "" ); // De même l'utilisateur ne se retrouve pas avec des valeurs non désirées lorsqu'il ajoute des piquets
        } else $( "#coords_" + i ).show( ); // Sinon on affiche le champs
}

$( function( ) {
    console.info( "JQuery chargée" ) // JQuery est une librairie Javascript permettant de se simplifier la vie, voir avec D. Durand pour plus d'informations
    $( '#nombre_piquet input' ).change( changeNbrPiquets ) // Si le contenu du champ de sélection du nombre de piquet change alors on appelle la fonction changeNbrPiquets
    changeNbrPiquets( ); // On appelle quand même la fonction pour des soucis de fonctionnalitées
    $( "#file_selector" ).change( importFromFile ); // De la même façon que 2 lignes au dessus mais pour l'importation de données par un fichier
    //$( ".coords input" ).change( draw ); // Décommenter cette ligne si vous souhaitez que le dessin se fasse à chaque changement de coordonnées (prend beaucoup de ressources)
} )

/**
 * Permet de compléter les champs donnant l'aire, le centre de gravité, et celui permettant de dire si la vache est dans le pré ou non, elle actualise aussi le canvas
 */
function showCalculs( ) {
    var aire = calculateAire( );
    $( "#aire" ).text( aire ); // Affiche le résultat du calcul de l'aire
    var gravite = calculateGravite( )
    $( "#centre" ).text( JSON.stringify( gravite ) ); // J'ai pas besoin de tout détailler si ? xD
    var cowIsInside = calculateInsidePolygon( )
    $( "#inside" ).text( cowIsInside );
    draw( ); // Appelle la fonction de dessin du canvas
}

/**
 * Permet de récupérer le nombre de piquets saisis par l'utilisateur
 * @return {number} Nombre de piquets
 * @throws {Error} Si le champs est mal renseigné, cela permet l'arrêt de l'exécution du code et d'éviter de potentielles erreurs plus loin dans le code
 */
function getNbPiquets( ) {
    if ( VerificationNombrePiquet( ) ) return $( '#nombre_piquet input' ).val( ); // Si la vérification est bonne alors on retourne la valeur du champs
    else throw new Error( "Le nombre de piquet est mal renseigné" ); // Sinon on envoie une erreur (une exception mais en Javascript)
}

/**
 * Permet de récupérer la position des piquets saisis par l'utilisateur
 * @return {Array<{x:number,y:number}>} Tableau contenant les positions de tous les piquets
 */
function getValues( ) {
    nbrPiquets = getNbPiquets( ); // On récupère le nombre de piquets pour pas récupérer trop d'infos
    coords = new Array( nbrPiquets ); // on défini un tableau de la bonne taille
    for ( i = 0; i < nbrPiquets; i++ ) {
        coords[ i ] = getPoint( i ); // On récupère toutes les coordonnées en les ajoutant dans le tableau
    }
    return coords; // on retourne le tableau complet s'il n'y a pas eu d'erreur provoquant un arrêt de la fonction
}

/**
 * Permet de récupérer la position des piquets saisis par l'utilisateur même si les valeurs ne sont pas complètes
 * @return {Array<{x:number,y:number}>} Tableau contenant les positions de tous les piquets disponibles
 */
function getAvailablesValues( ) {
    nbrPiquets = getNbPiquets( ); // Tout comme la fonction précédente on récupère le nombre de piquets prévus
    coords = [ ]; // Cette fois-ci on initialise un tableau vide parce qu'on ne sais pas jusqu'où on ira dans les valeurs
    for ( i = 0; i < nbrPiquets; i++ ) {
        try {
            coords.push( getPoint( i ) ); // On essaie de récupérer les coordonnées de chaque points
        } catch ( err ) {
            break; // Si on a une erreur on arrête là
        }
    }
    return coords; // On renvoi les coordonnées qu'on a pu récupérer
}

/**
 * Permet de récupérer la position du piquet index saisi par l'utilisateur
 * @param {number} index - Index du piquet à récupérer
 * @return {{x:number,y:number}} Position du piquet index
 * @throws {Error} Si le champs est mal renseigné, cela permet l'arrêt de l'exécution du code et d'éviter de potentielles erreurs plus loin dans le code
 */
function getPoint( index ) {
    while ( index >= getNbPiquets( ) ) index -= getNbPiquets( ); // Si jamais l'index est supérieur ou égal au nombre de piquets on fait en sorte qu'il revienne dans l'ensemble correct
    if ( VerificationPiquet( index ) ) { // On vérifie que les champs soient bien remplis
        xCoord = Number( $( "#coord_" + index + "_x" ).val( ) ); // On récupère les valeurs qu'on converti en number (nombre réel)
        yCoord = Number( $( "#coord_" + index + "_y" ).val( ) );
        return { x: xCoord, y: yCoord }; // On retourne ces valeurs sous la forme d'un dictionnaire
    } else throw new Error( "Les coordonnées du piquet n°" + index + " sont mal renseignées" ); // Si la vérification est mauvaise alors on envoie une erreur
}

/**
 * Permet de compléter les différents champs grace à un fichier, fonction asynchrone !
 */
function importFromFile( ) {
    if ( $( "#file_selector" ).prop( "files" ).length == 0 ) return; // Si aucun fichier n'est sélectionné alors ça sert à rien de continuer
    file = $( "#file_selector" ).prop( "files" )[ 0 ]; // Le fichier qu'on veut est le premier de ceux sélectionnées s'il y en a plusieurs (sauf si l'utilisateur est un petit coquin il ne devrait y en avoir qu'un seul)
    let reader = new FileReader( );
    reader.addEventListener( 'load', function( e ) { // Fonction à exécuter quand le fichier sera chargé
        let text = e.target.result; // On récupère le contenu du fichier
        switch ( file.type ) { // Selon le type MIME du fichier on agit différement
            case "application/json":
                importFromJSON( text );
                break;
            case "text/plain":
                importFromTXT( text );
                break;
        }
        changeNbrPiquets( ); // Après la modification des champs on appelle les fonctions nécessaires 
        showCalculs( );
    } );
    reader.readAsText( file ); // On lance le chargement du fichier
}

/**
 * Permet de compléter les différents champs grace à un fichier JSON
 * @param {string} text - Contenu encodé en texte du fichier
 */
function importFromJSON( text ) {
    imported = JSON.parse( text ); // Le plus simple en javascript c'est de gérer du JSON (c'est de là que ça vient ;) )
    if ( imported.length > 50 ) // On veut pas plus de 50 piquets
        imported.length = 50;
    $( "#nombre_piquet input" ).val( imported.length ); // On remplit le champs de nombre de piquets
    for ( i = 0; i < imported.length; i++ ) {
        $( "#coord_" + i + "_x" ).val( imported[ i ].x ); // Pareil pour les coordonnées
        $( "#coord_" + i + "_y" ).val( imported[ i ].y );
    }
}

/**
 * Permet de compléter les différents champs grace à un fichier TXT
 * @param {string} text - Contenu encodé en texte du fichier
 */
function importFromTXT( text ) {
    imported = text.split( "\n" ); // On sépare chaque lignes dans un tableau
    $( "#nombre_piquet input" ).val( imported[ 0 ] ); // On remplit les champs
    for ( i = 1; i < ( imported[ 0 ] * 2 ) || i < 100; i += 2 ) {
        $( "#coord_" + parseInt( i / 2 ) + "_x" ).val( imported[ i ] );
        $( "#coord_" + parseInt( i / 2 ) + "_y" ).val( imported[ i + 1 ] );
    }
}
