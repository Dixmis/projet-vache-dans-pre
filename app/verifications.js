/**
 * Verifier que le champ de sélection du nombre de piquet est conforme au format attendu (entier entre 3 et 50)
 * @return {boolean} Si aucune erreur n'est détectée --> True
 * Si une erreur est détectée --> False
 */
function VerificationNombrePiquet( ) {
    value = $( '#nombre_piquet input' ).val( ).trim( );
    if ( value === '' ) {
        $( '#nombre_piquet span' ).hide( )
        $( '#nombre_piquet #empty' ).show( )
        $( '#nombre_piquet' ).addClass( 'erreur' )
        return false
    } else {
        value = parseInt( value, 10 )
        if ( isNaN( value ) || value < 3 || value > 50 ) {
            $( '#nombre_piquet span' ).hide( )
            $( '#nombre_piquet #number' ).show( )
            $( '#nombre_piquet' ).addClass( 'erreur' )
            return false
        } else {
            $( '#nombre_piquet span' ).hide( )
            $( '#nombre_piquet' ).removeClass( 'erreur' )
            return true
        }
    }

}

/**
 * Verifier que les champs de sélection des coordonnées des piquets sont correctement renseignés
 * @param {number} index - index du champ nombre parmis l'ensemble de champs disponible
 * @return {boolean} Si aucune erreur n'est détectée --> True
 * Si une erreur est détectée --> False
 */
function VerificationPiquet( index ) {
    coordX = $( "#coord_" + index + "_x" );
    coordY = $( "#coord_" + index + "_y" );
    div = $( "#coords_" + index );
    if ( coordX.val( ).trim( ) === '' || coordY.val( ).trim( ) === '' ) {
        console.log( "champs vides" );
        div.addClass( 'erreur' );
        return false;
    } else {
        if ( isNaN( coordX.val( ).trim( ) ) || isNaN( coordY.val( ).trim( ) ) ) {
            console.log( "NaN" );
            div.addClass( 'erreur' );
            return false;
        } else {
            div.removeClass( 'erreur' );
            return true;
        }
    }
}
